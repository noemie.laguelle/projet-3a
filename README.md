# Bac à sable pour la faille Zerologon

## Contexte

Dans le cadre de ma dernière année aux Mines de Nancy, j'ai travaillé sur la faille Zerologon et plus particulièrement sur le développement d'un environnement permettant d'exploiter cette faille et de la détecter. 

## Déploiement 

1. Cloner le dépôt

2. Dans le dépôt :  ```cd splunk_server/``` puis ``` vagrant up  ```

3. Toujour dans le dépôt : ```cd windows_dc_and_attacker/``` puis ```vagrant up ```

## Utilisation

Vous avez trois machines à votre disposition : 

- Un Windows Server 2019 contrôleur de domaine Active Directory (compte administrateur : administrator/Admin*).

- Une Kali qui permet d'attaquer (identifiants **vagrant/vagrant**) avec sur le bureau, un clone du dépôt https://github.com/SecuraBV/CVE-2020-1472 permettant de tester si le contrôleur de domaine est vulnérable et le dossier Impacket contenant les fonctions nécessaires à l'exploitation de la vulnérabilité. Vous pouvez utiliser l'exploit que vous souhaitez pour l'attaque. Personnellement, j'ai utilisé celui-ci : https://github.com/risksense/zerologon . 

- Une Debian 9 (identifiants vagrant/vagrant) avec un serveur Splunk. L'interface web de Splunk est accessible à l'adresse **10.0.1.12:8000** depuis l'hôte avec les identifiants : admin/0logonAtt*ck .


## Sources 

https://github.com/d1vious/building-a-windows-dc


https://github.com/splunk/attack_range_local
